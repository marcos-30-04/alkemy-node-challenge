const Sequelize = require("sequelize");

const UserModel = require("./models/users");
const MovieModel = require("./models/movies");
const CharacterModel = require("./models/characters");
const GenreModel = require("./models/genres");

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "./database/database.sqlite",
});

const User = UserModel(sequelize, Sequelize);
const Movie = MovieModel(sequelize, Sequelize);
const Character = CharacterModel(sequelize, Sequelize);
const Genre = GenreModel(sequelize, Sequelize);

Genre.hasMany(Movie, { foreignKey: "genreId" });
Movie.belongsTo(Genre);

Movie.belongsToMany(Character, {
  through: "MovieCharacters",
  as: "characters",
});
Character.belongsToMany(Movie, { through: "MovieCharacters", as: "movies" });

sequelize.sync({ force: false }).then(() => {
  console.log("Tablas sincronizadas");
});

module.exports = {
  Movie,
  User,
  Character,
  Genre,
};
