module.exports = (sequelize, type) => {
  return sequelize.define("movie", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    img: type.TEXT,
    title: type.TEXT,
    date: type.INTEGER,
    score: type.INTEGER,
  });
};
