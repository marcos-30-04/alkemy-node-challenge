// loads data into the databases
const sequelize = require("./database/db");
const { Character } = require("./database/db");
const { Movie } = require("./database/db");
const { Genre } = require("./database/db");

// Characters
const characters = [
  {
    img: "img.jpg",
    name: "Aladdin",
    age: 18,
    weight: 65,
    story: "thief",
  },
  {
    img: "img.jpg",
    name: "Jafar",
    age: 45,
    weight: 54,
    story: "bad guy",
  },
  {
    img: "img.jpg",
    name: "Jasmine",
    age: 17,
    weight: 46,
    story: "princess",
  },
  {
    img: "img.jpg",
    name: "Genie",
    age: 3000,
    weight: 1,
    story: "un capo",
  },
  {
    img: "img.jpg",
    name: "Tarzan",
    age: 25,
    weight: 75,
    story: "Half monkey",
  },
  {
    img: "img.jpg",
    name: "Jane",
    age: 20,
    weight: 65,
    story: "rich girl",
  },
  {
    img: "img.jpg",
    name: "Robin Hood",
    age: 22,
    weight: 20,
    story: "good guy",
  },
  {
    img: "img.jpg",
    name: "Friar Tuck",
    age: 15,
    weight: 12,
    story: "good guy friend of good guy",
  },
  {
    img: "img.jpg",
    name: "Maid Marian",
    age: 18,
    weight: 16,
    story: "good gal",
  },
  {
    img: "img.jpg",
    name: "Simba",
    age: 18,
    weight: 80,
    story: "good lion",
  },
  {
    img: "img.jpg",
    name: "Scar",
    age: 30,
    weight: 70,
    story: "bad lion",
  },
  {
    img: "img.jpg",
    name: "Nala",
    age: 18,
    weight: 60,
    story: "good ¿lioness? ¿fe-lion? feline",
  },
  {
    img: "img.jpg",
    name: "Timon",
    age: 14,
    weight: 3,
    story: "little good guy",
  },
  {
    img: "img.jpg",
    name: "Pumbaa",
    age: 15,
    weight: 60,
    story: "big good guy",
  },
  {
    img: "img.jpg",
    name: "Fa Mulan",
    age: 19,
    weight: 65,
    story: "chinese redemptioner against the huns",
  },
  {
    img: "img.jpg",
    name: "Mushu",
    age: 2000,
    weight: 2,
    story: "cool spirit",
  },
  {
    img: "img.jpg",
    name: "Milo Thatch",
    age: 26,
    weight: 60,
    story: "archaeologist",
  },
  {
    img: "img.jpg",
    name: "Commander Lyle",
    age: 34,
    weight: 70,
    story: "can't remember who he is ",
  },
  {
    img: "img.jpg",
    name: "Kida",
    age: 27,
    weight: 60,
    story: "demi godess?",
  },
  // {img: "img.jpg', name: "", age: , weight: , story: "" }
];

// movies
const movies = [
  {
    img: "img.jpg",
    title: "Aladdin",
    date: 1992,
    score: 5,
    genreId: 1,
  },
  {
    img: "img.jpg",
    title: "The Return of Jafar",
    date: 1994,
    score: 3,
    genreId: 1,
  },

  {
    img: "img.jpg",
    title: "Tarzan",
    date: 1999,
    score: 4,
    genreId: 2,
  },
  {
    img: "img.jpg",
    title: "Tarzan & Jane",
    date: 2002,
    score: 3,
    genreId: 2,
  },
  {
    img: "img.jpg",
    title: "Tarzan II",
    date: 2005,
    score: 2,
    genreId: 2,
  },
  {
    img: "img.jpg",
    title: "Robin Hood",
    date: 1973,
    score: 5,
    genreId: 3,
  },
  {
    img: "img.jpg",
    title: "The Lion King",
    date: 1994,
    score: 5,
    genreId: 1,
  },
  {
    img: "img.jpg",
    title: "The Lion King II",
    date: 1998,
    score: 3,
    genreId: 1,
  },
  {
    img: "img.jpg",
    title: "The Lion King 1/2 (seems desperate)",
    date: 2004,
    score: 1,
    genreId: 1,
  },
  {
    img: "img.jpg",
    title: "Mulan",
    date: 1998,
    score: 4,
    genreId: 4,
  },
  {
    img: "img.jpg",
    title: "Atlantis",
    date: 2001,
    score: 5,
    genreId: 1,
  },
  // {img: "img.jpg', title: "", date: ,  score: , genreId: , }
];

// Genres (not so ingenious)

const genres = [
  { name: "magical", img: "genreImage.jpg", movieIds: { ids: [1, 2, 11] } },
  {
    name: "nature",
    img: "genreImage.jpg",
    movieIds: { ids: [3, 4, 5, 7, 8, 9] },
  },
  { name: "medieval", img: "genreImage.jpg", movieIds: { ids: [6] } },
  { name: "chinese", img: "genreImage.jpg", movieIds: { ids: [10] } },
  // {name: "", img:"genreImage.jpg", movieIds: [ids: []]}
];

const populateDatabase = async () => {
  // Create All Movies
  await Movie.bulkCreate(movies).catch((err) => console.log(err));

  // Create All Characters
  await Character.bulkCreate(characters).catch((err) => console.log(err));

  // Create All Genres
  await Genre.bulkCreate(genres).catch((err) => console.log(err));
};

const establishAssociations = async () => {
  // Find Each Movie
  const aladdin = await Movie.findByPk(1);
  const theReturnOfJafar = await Movie.findByPk(2);
  const tarzan = await Movie.findByPk(3);
  const tarzanandjane = await Movie.findByPk(4);
  const tarzantwo = await Movie.findByPk(5);
  const robinHood = await Movie.findByPk(6);
  const theLionKing = await Movie.findByPk(7);
  const theLionKingII = await Movie.findByPk(8);
  const theLionKinghalf = await Movie.findByPk(9);
  const mulan = await Movie.findByPk(10);
  const atlantis = await Movie.findByPk(11);

  // Find Each Character
  const aladdinCharacter = await Character.findByPk(1);
  const jafar = await Character.findByPk(2);
  const jasmine = await Character.findByPk(3);
  const genie = await Character.findByPk(4);
  const tarzanCharacter = await Character.findByPk(5);
  const jane = await Character.findByPk(6);
  const robinHoodCharacter = await Character.findByPk(7);
  const friarTuck = await Character.findByPk(8);
  const maidMarian = await Character.findByPk(9);
  const simba = await Character.findByPk(10);
  const scar = await Character.findByPk(11);
  const nala = await Character.findByPk(12);
  const timon = await Character.findByPk(13);
  const pumbaa = await Character.findByPk(14);
  const faMulan = await Character.findByPk(15);
  const mushu = await Character.findByPk(16);
  const milo = await Character.findByPk(17);
  const commanderLyle = await Character.findByPk(18);
  const kida = await Character.findByPk(19);

  // Find Each Genre
  const magical = await Genre.findByPk(1);
  const nature = await Genre.findByPk(2);
  const medieval = await Genre.findByPk(3);
  const chinese = await Genre.findByPk(4);

  // set movies for each genre
  await magical.setMovies([aladdin, theReturnOfJafar, atlantis]);
  await nature.setMovies([
    tarzan,
    tarzanandjane,
    tarzantwo,
    theLionKing,
    theLionKingII,
    theLionKinghalf,
  ]);
  await medieval.setMovies([robinHood]);
  await chinese.setMovies([mulan]);

  // set Characters for each movie;
  await aladdin.setCharacters([aladdinCharacter, jafar, jasmine, genie]);
  await theReturnOfJafar.setCharacters([
    aladdinCharacter,
    jafar,
    jasmine,
    genie,
  ]);
  await tarzan.setCharacters([tarzanCharacter, jane]);
  await tarzanandjane.setCharacters([tarzanCharacter, jane]);
  await tarzantwo.setCharacters([tarzanCharacter, jane]);
  await robinHood.setCharacters([robinHoodCharacter, friarTuck, maidMarian]);
  await theLionKing.setCharacters([simba, scar, nala, timon, pumbaa]);
  await theLionKingII.setCharacters([simba, scar, nala, timon, pumbaa]);
  await theLionKinghalf.setCharacters([simba, scar, nala, timon, pumbaa]);
  await mulan.setCharacters([faMulan, mushu]);
  await atlantis.setCharacters([milo, commanderLyle, kida]);
};

module.exports = { populateDatabase, establishAssociations };
