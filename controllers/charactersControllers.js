const { Character } = require("../database/db");

//  GET '/'
const allCharacters = (req, res) => {
  const query = req.query;

  // Response with queries  (a function should be made here!)
  if (query.name) {
    Character.findAll({
      where: { name: query.name },
      include: {
        association: "movies",
        attributes: ["title"],
      },

      attributes: ["img", "name", "age", "weight", "story"],
    })
      .then((character) => res.json(character))
      .catch((err) => res.status(400).json("Error: " + err));
  } else if (query.age) {
    Character.findAll({
      where: { age: query.age },
      include: {
        association: "movies",
        attributes: ["title"],
      },
      attributes: ["img", "name", "age", "weight", "story"],
    })
      .then((character) => res.json(character))
      .catch((err) => res.status(400).json("Error: " + err));
  } else if (query.weight) {
    Character.findAll({
      where: { weight: query.weight },
      include: {
        association: "movies",
        attributes: ["title"],
      },
      attributes: ["img", "name", "age", "weight", "story"],
    })
      .then((character) => res.json(character))
      .catch((err) => res.status(400).json("Error: " + err));
  } else if (query.movies) {
    Character.findAll({
      include: {
        association: "movies",
        attributes: [],
        where: { id: query.movies },
      },
    })
      .then((characters) => res.json(characters))
      .catch((err) => res.status(400).json("Error: " + err));
  }

  // Default Response
  Character.findAll({ attributes: ["img", "name"] })
    .then((characters) => {
      res.json(characters);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

//  POST '/'
const addCharacter = (req, res) => {
  Character.create(req.body)
    .then((character) => res.json(character))
    .catch((err) => res.status(400).json("Error: " + err));
};

// GET '/:characterId'
const getCharacterById = (req, res) => {
  Character.findByPk(req.params.characterId, {
    include: {
      association: "movies",
      aatributes: ["title"],
    },
    attributes: ["img", "name", "age", "weight", "story"],
  }).then((character) => {
    res.json(character);
  });
};

// PUT '/:characterId'
const updateCharacter = (req, res) => {
  Character.update(req.body, {
    where: { id: req.params.characterId },
  })
    .then(() => {
      res.json(`You've edited the character successfully`);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

// DELETE '/:characterId'

const deleteCharacter = (req, res) => {
  Character.destroy({
    where: { id: req.params.characterId },
  })
    .then(() => {
      res.json(`You've deleted the character successfully`);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

module.exports = {
  allCharacters,
  addCharacter,
  getCharacterById,
  updateCharacter,
  deleteCharacter,
};
