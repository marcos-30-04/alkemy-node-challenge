const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const msg = (email) => {
  return {
    to: `${email}`,
    from: "marcosfunesberdos@gmail.com",
    subject: "Thank you for using Disney API",
    text: "This is a thank you email in regard your passion to know it all about the unimaginary characters of the Disney Universe",
    html: "<strong> thank you email from SendGrid</strong>",
  };
};

const sendEmail = (email) => {
  sgMail
    .send(msg(email))
    .then(() => {
      console.log("Email sent");
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = sendEmail;
