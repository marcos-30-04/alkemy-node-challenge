const { Movie, Genre, Character } = require("../database/db");

// GET '/'
const allMovies = (req, res) => {
  const query = req.query;

  // Response with queries
  if (query.name) {
    Movie.findOne({
      where: { title: query.name },
      include: {
        association: "characters",
        attributes: ["img", "name"],
      },
      attributes: ["img", "title", "date", "score"],
    })
      .then((movie) => res.json(movie))
      .catch((err) => res.status(400).json("Error: " + err));
  } else if (query.genre) {
    Genre.findByPk(query.genre, {
      include: {
        model: Movie,
        attributes: ["img", "title"],
      },
      attributes: ["img", "name"],
    })
      .then((genre) => res.json(genre))
      .catch((err) => res.status(400).json("Error: " + err));
  } else if (query.order) {
    Movie.findAll({ attributes: ["img", "title", "date"] }).then((movies) => {
      if (query.order === "DESC") {
        const sortedDesc = movies.sort((a, b) => b.date - a.date);
        res.json(sortedDesc);
      } else if (query.order === "ASC") {
        const sortedAsc = movies.sort((a, b) => a.date - b.date);
        res.json(sortedAsc);
      }
    });
  }
  // Default response
  Movie.findAll({ attributes: ["img", "title", "date"] }).then((movies) => {
    res.json(movies);
  });
};

// POST '/'
const addMovie = (req, res) => {
  Movie.create(req.body)
    .then((film) => {
      res.json(film);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

// GET '/:movieId'
const getMovieById = (req, res) => {
  Movie.findByPk(req.params.movieId, {
    include: {
      association: "characters",
      attributes: ["img", "name"],
    },
    attributes: ["img", "title", "date", "score"],
  })
    .then((movies) => {
      res.json(movies);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

// PUT '/:movieId'
const updateMovie = (req, res) => {
  Movie.update(req.body, {
    where: { id: req.params.movieId },
  })
    .then(() => {
      res.status(201).json(`You've edited the movie successfully`);
    })
    .catch((err) => {
      res.status(400).json("Error: " + err);
    });
};

//DELETE '/:movieId'
const deleteMovie = (req, res) => {
  Movie.destroy({ where: { id: req.params.movieId } })
    .then(() => {
      res.status(200).json("Movie deleted successfully.");
    })
    .catch((err) => {
      res.status(400).json("Error: " + err);
    });
};

module.exports = {
  allMovies,
  addMovie,
  getMovieById,
  updateMovie,
  deleteMovie,
};
