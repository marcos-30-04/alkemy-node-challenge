require("dotenv").config();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../database/db");
const sendEmail = require("./sendGrid");

// POST '/register'
const register = async (req, res) => {
  // check email is not registered, password is valid, send welcome email
  let { email, password, username } = req.body;

  if (!email || !username || !password) {
    return res.status(400).send("You must send email, username and password");
  }

  const isRegistered = await User.findOne({ where: { email } });

  if (isRegistered) {
    return res.status(406).send("Email is already registered");
  }

  if (password.length < 5) {
    return res.status(406).send("Password must have 5 characters or more");
  }

  password = bcrypt.hashSync(password);

  sendEmail(email);

  User.create({ email, username, password })
    .then((user) => res.send(user))
    .catch((err) => res.status(400).send(err));
};
const login = async (req, res) => {
  // check email, password, send token
  const databaseUser = await User.findOne({ where: { email: req.body.email } });

  if (!databaseUser) {
    return res.send("user email or password are incorrect");
  }
  const correctPassword = bcrypt.compareSync(
    req.body.password,
    databaseUser.password
  );

  if (!correctPassword) {
    return res.send("user email or password are incorrect");
  }
  if (correctPassword) {
    const user = { name: databaseUser.username };

    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
    res.json({ accessToken, user: user.name });
  }
};

module.exports = {
  register,
  login,
};
