# Alkemy Node Challenge

    API which allows the user to know and modify Disney movies and its characters.

## Cloning

    Clone the repository and install npm dependencies

## To see Endpoints Documentation

    You must run 'npx serve Insomnia\ documentation/ -l 5001' on the root directory and open 'http://localhost:5001' on your browser

## To start the API Server

    Run 'npm start'
    The server is available at localhost:5000

### To seed and establish associations in the database

    Make a POST request to:

    localhost:5000/populate

    for some reason i have not been able to solve it might take two tries.
    to be sure you can make a GET request to '/movies', if no movies are being returned, the next time the '/populate' request should fill the movies table.

    and then make a POST request to

    localhost:5000/associateDatabase
