const router = require("express").Router();

const {
  allMovies,
  addMovie,
  getMovieById,
  updateMovie,
  deleteMovie,
} = require("../../controllers/moviesControllers");

router.get("/", allMovies);

router.post("/", addMovie);

router.get("/:movieId", getMovieById);

router.put("/:movieId", updateMovie);

router.delete("/:movieId", deleteMovie);

module.exports = router;
