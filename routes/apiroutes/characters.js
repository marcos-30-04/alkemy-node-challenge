const router = require("express").Router();

const {
  allCharacters,
  addCharacter,
  getCharacterById,
  updateCharacter,
  deleteCharacter,
} = require("../../controllers/charactersControllers");

router.get("/", allCharacters);

router.post("/", addCharacter);

router.get("/:characterId", getCharacterById);

router.put("/:characterId", updateCharacter);

router.delete("/:characterId", deleteCharacter);

module.exports = router;
