const router = require("express").Router();

const authenticateToken = require("./authMiddleware");
const charactersRouter = require("./apiroutes/characters");
const moviesRouter = require("./apiroutes/movies");
const { populateDatabase, establishAssociations } = require("../seed");

router.use("/characters", authenticateToken, charactersRouter);
router.use("/movies", authenticateToken, moviesRouter);

router.get("/", (req, res) => {
  res.send("Alkemy API");
});

router.post("/populate", (req, res) => {
  const response = populateDatabase();
  res.json(response);
});

router.post("/associateDatabase", (req, res) => {
  const response = establishAssociations();
  res.json(response);
});

module.exports = router;
